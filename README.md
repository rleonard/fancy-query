#FancyQuery.js
A simple JavaScript autocomplete library that supports multiple syntax layers (also called search tokens).

Licensed under the MIT license.  See LICENSE.txt for more details.

The user interface is intended to be a blend of StackExchange and Apple's Finder search; searches are done with a basic syntax like StackExchange, where various options can be defined with markup such as "tag:<tag name>", and allow for an auto-complete system such as the one pictured under "Search tokens" at [Apple's website](http://support.apple.com/kb/HT2470).

## Installation
The entire plugin is in one file, FancyQuery.js.  
FancyQuery.js depends on [jQuery](http://jquery.com/) being installed, and plays nicely with Bootstrap.  
Additional styling can be added with CSS, using `!important` to override the built in styling.

## Usage
All FancyQuery requires is a text input field - it builds the rest of the user interface itself.

	<input type="text" id="mainSearch" />

In JavaScript, create a new `FancyQuery` instance.

	<script>
		var search = new FancyQuery("#mainSearch", tags);
	</script>

The tags array defines what tokens to autocomplete.
The array should contain a series of objects, each containing the name of the tag, a short description, the different commands to listen for, and where to get autocomplete data from.
An example of a tags array is below.

	<script>
		var tags = [
			"Type": {
				name: "Document Type",
				description: "Require a specific document type",
				commands: ["type:", "+type:"],
				autoCompleteSource: doctypes
			},
			"BannedTypes": {
				name: "Banned Document Type",
				description: "Ban a specific document type",
				commands: ["-type:", "type:-", "!type:", "type:!"],
				autoCompleteSource: doctypes
			},
			"CreatedSince": {
				name: "Created Since",
				description: "Require documents to have been created since the specified date.",
				commands: ["since:", "after:", "newerThan:", "createdSince:", "createdAfter:"],
				autoCompleteSource: dateFunction
			}
		];
	</script>

The name and descriptions are displayed when the user is typing a new command.  
The commands array defines the various strings to listen for.  
The autoCompleteSource can be an array, function, or string to XML, JSON, or CSV file.  
If the autoCompleteSource is an array, the values will be suggested as the user types in the field.  
A function in the autoCompleteSource will be called, with the current string the user is typing passed as an argument.  
An autoCompleteSource URL (as a string) will be POSTed to, with the current string being typed passed as `$_POST["value"]` (in PHP). The URL needs to return a CONTENT-TYPE in the headers, otherwise the request might fail.