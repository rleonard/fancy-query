/*
	FancyQuery.js
	Copyright (c) 2013, Ryan Leonard
	Licenced under the MIT Licence.  See LICENCE.txt for more information.

	Sample Usage:
	<input type="text" id="fancySearch" />
	var mainSearch = new FancyQuery("#fancySearch", [
		"Tag": {
			name: "Tag",
			description: "Require a specific tag",
			commands: ["tag:", "+tag:"],
			ignore: ["tags:-", "tags:!"], // currently doesn't do anything
			autoCompleteSource: getTags // Can be a function (will invoke with the start of the string), string with url to xml/json/csv (MAKE SURE TO USE PROPER CONTENT-TYPE), or array
		}
	]);
*/
Array.prototype.extend = function(array) {
	for (var i = 0, len = array.length; i < len; ++i) {
    	this.push(array[i]);
	};
};
/**
 * Returns all entries in an array that start with the value provided
 * @param {String} c The string to search for
 * @returns {Array) All entries that match (empty if there are no matches)
*/
Array.prototype.startWith = function(c) {
	var result = [];
	for(var i=0, len=this.length; i<len; i++){
		if(this[i].indexOf(c) == 0){
			result.push(this[i]);
		}
	}
	return result;
};

function Commands() {}
/**
 * Returns all entries in an object that start with the value provided
 * @param {String} c The string to search for
 * @param {Boolean} useKeys Use item keys instead of values
 * @returns {Object} All entries that match (empty if there are no matches)
*/
Commands.prototype.startWith = function(c, useKeys) {
	var result = {};
	for(var item in this) {
		var matchAgainst = useKeys ? item : this[item];
		if(this.hasOwnProperty(item) && matchAgainst.indexOf(c) == 0) {
			result[item] = this[item];
		}
	}
	return result;
}
/**
 * Returns all entries in an array that are at the beginning of the provided string
 * @param {String} c The string to search for
 * @param {Boolean} useKeys Use item keys instead of values
 * @param {Boolean} max Only return the item with the largest key
 * @param {Array} All entries that match (empty if there are no matches)
*/
Commands.prototype.atStartOf = function(c, useKeys, max) {
	var result = {};
	for(var item in this) {
		var matchAgainst = useKeys ? item : this[item];
		if(this.hasOwnProperty(item) && c.indexOf(matchAgainst) === 0) {
			result[item] = this[item];
		}
	}
	if(max) {
		var largest = "";
		for(var item in result) {
			if(result.hasOwnProperty(item) && item.length > largest.length) {
				largest = item;
			}
		}
		maxresult = {};
		maxresult[largest] = result[largest];
		result = maxresult;
	}
	return result;
}

/**
 * Links to an <input /> field and adds fancy auto-complete magic.
 * @constructor
 * @param {String} selector - The selector of an element to link to (must be an <input type="text" />)
 * @param {Array} tags - The different tags to listen to
*/
function FancyQuery(selector, tags) {
	/**
	 * The "fake" constructor function - called at the end of the full class definition.
	 * Takes the same arguments as the main class, which are directly passed on.
	 * @param {String} selector - The selector of an element to link to (must be an <input type="text" />)
 	 * @param {Array} tags - The different tags to listen to
 	 * @returns {FancyQuery}
 	*/
	this.construct = function(selector, tags) {
		this.selector = selector;
		this.tags = tags;
		this.commands = new Commands();
		// add all commands to this.commands
		for (var tagName in tags) {
			if (tags.hasOwnProperty(tagName)) {
				var tag = tags[tagName];
				for (var i = tag.commands.length - 1; i >= 0; i--) {
					this.commands[tag.commands[i]] = tagName;
				};
			}
		};
		this.buildGUI().addListeners();
		return this;
	}
	/**
	 * GUI builder - adds a div for results after the <input />
	 * @returns {FancyQuery}
	*/
	this.buildGUI = function() {
		// Setup results div
		$(this.selector).after('<div class="fancyQueryResults"></div>');
		$(this.selector).next().css({
			"display": "none",
			"position": "absolute",
			"z-index": 100,
			"background-color": "white",
			"font-size": "10pt",
			"padding": "5px"
		});
		return this;
	}
	/**
	 * Adds listeners/callbacks to the main <input />
	 * @returns {FancyQuery}
	*/
	this.addListeners = function() {
		$(this.selector).on("click", {"obj": this}, this.inputClicked);
		$(this.selector).on("keyup", {"obj": this}, this.keyPressed);
		$(this.selector).on("blur", {"obj": this}, this.lostFocus);
		$(this.selector).next().on("click", {"obj": this}, this.inputClicked);
		$(this.selector).on("forceShow", {"obj": this}, function(ev) {
			var obj = ev.data.obj;
			obj.showResults(true);
			obj.showResults();
		});
		return this;
	}
	/**
	 * Called when the main input is clicked on.
	 * Allows continuing of a previous tag, if the user unfocuses or
	 * switches to a different tag.
	*/
	this.inputClicked = function(ev) {
		var obj = ev.data.obj;
		obj.updateResults();
	}
	/**
	 * Called when the user presses a key in the input.
	 * Allows checking for tag results.
	*/
	this.keyPressed = function(ev) {
		var obj = ev.data.obj;
		obj.updateResults();
	}
	/**
	 * Called when the user unfocuses the input.
	 * Allows hiding of search results.
	*/
	this.lostFocus = function(ev) {
		var obj = ev.data.obj;
		obj.hideResults();
	}
	/**
	 * Updates the search results shown.
	 * @returns {FancyQuery}
	*/
	this.updateResults = function() {
		this.hideResults(true).clearResults();
		var cursorPos = $(this.selector)[0].selectionStart;
		// cancel if more than one position is selected
		if($(this.selector)[0].selectionEnd !== cursorPos) {
			return this;
		}
		var val = $(this.selector).val();
		var upToCursor = val.substr(0, cursorPos);
		var tag = upToCursor.replace(/^.*?([^\s]+(?:"[^"]*")?)$/, "$1");
		if(tag == "") {
			return this;
		}

		// try to complete tag
		completeTags = this.commands.startWith(tag, true);
		for(var commandAlias in completeTags) {
			if(this.commands.hasOwnProperty(commandAlias)) {
				var commandName = this.commands[commandAlias];
				var tagData = this.tags[commandName];
				this.addTagResult(commandName, commandAlias, tagData, tag, cursorPos);
			}
		}

		// try to provide results for longest tag
		for(var commandAlias in this.commands.atStartOf(tag, true, true)) {
			if(this.commands.hasOwnProperty(commandAlias)) {
				var commandName = this.commands[commandAlias];
				var tagData = this.tags[commandName];
				var tagValue = tag.replace(commandAlias, "");
				this.autoCompleteTag(commandName, commandAlias, tagData, tag, cursorPos);
			}
		}
		this.showResults(true);
		return this;
	}
	/**
	 * Suggest a tag for the user as an autocomplete option
	 * (overrides if the tag is already being suggested)
	 * @param {String} commandName The command being called
	 * @param {String} commandAlias The alias the user is typing
	 * @param {String} tagData The current section of this.tags
	 * @param {String} alreadyWritten The tag up to where the user has typed
	 * @param {Integer} cursorPos The current position of the cursor
	*/
	this.addTagResult = function(commandName, commandAlias, tagData, alreadyWritten, cursorPos) {
		if(!$(this.selector).next().has(".sectionTitle[data-type='tags']").length) {
			$(this.selector).next().prepend('<div class="sectionTitle" data-type="tags">Tags</div>');
			$(this.selector).next().children("[data-type='tags']").css({
				"background-color": "grey",
				"color": "white",
				"padding": "2px",
				"padding-left": "5px"
			});
		}
		if($(this.selector).next().children(".autoCompleteResult[data-type='tags'][data-commandName='"+commandName+"']").length) {
			var result = $(this.selector).next().children(".autoCompleteResult[data-type='tags'][data-commandName='"+commandName+"']");
			result.children(".commandAlias").text(commandAlias);
		}
		else {
			var result = $("<div />");
			result.addClass("autoCompleteResult").attr("data-type", "tags").attr("data-commandName", commandName);
			result.append($("<div />").text(commandAlias).addClass("commandAlias muted").css("float", "right"));
			result.append($("<div />").text(tagData["name"]).addClass("commandTitle"));
			result.append($("<p />").text(tagData["description"]).addClass("commandDescription"));
			// for replacement method: result.data("replaceText", alreadyWritten).data("insertText", commandAlias);
			// for insertion method:
			result.data("insertText", commandAlias.replace(alreadyWritten, "")).data("cursor", cursorPos);
			$(this.selector).next().children(".sectionTitle[data-type='tags']").after(result);
			$(this.selector).next().children(
				".autoCompleteResult[data-type='tags'][data-commandName='"+commandName+"']"
			).click(function(ev) {
				ev.preventDefault();
				// for replacement method:
				//var replaceFind = $(this).data("replaceText");
				//var replaceWith = $(this).data("insertText");
				// $(this).parent().prev().val($(this).parent().prev().val().replace(replaceFind, replaceWith));
				// for insertion method:
				var newText = $(this).data("insertText");
				var cursorPos = $(this).data("cursor");
				var input = $(this).parent().prev();
				var string = input.val();
				input.val(string.substr(0,cursorPos) + newText + string.substr(cursorPos));
				$(this).parent().prev().focus().trigger("forceShow");
				//return false;
			});
		}
	}
	/**
	 * Autocomplete a tag that has been fully entered by the user
	 * @param {String} commandName The command being called
	 * @param {String} commandAlias The alias the user is typing
	 * @param {String} tagData The current section of this.tags
	 * @param {String} alreadyWritten The tag up to where the user has typed
	 * @param {Integer} cursorPos The current position of the cursor
	*/
	this.autoCompleteTag = function(commandName, commandAlias, tagData, alreadyWritten, cursorPos) {
		var autoCompleteSource = tagData["autoCompleteSource"];
		var tagValue = alreadyWritten.replace(commandAlias, "");
		var completeValues = [];
		if(typeof(autoCompleteSource) === "object") {
			var values = autoCompleteSource.startWith(tagValue);
			for (var i = values.length - 1; i >= 0; i--) {
				completeValues.push(values[i]);
			};
		}
		else if(typeof(autoCompleteSource) === "string") {
			return;
			$.ajax({
				url: autoCompleteSource,
				type: "POST",
				async: false,
				data: {
					"value": tagValue
				},
				success: function(response, status, xhr) {
					var ct = xhr.getResponseHeader("content-type") || "";
					if(ct.indexOf('xml') > -1) {
						$(response).find("value").each(function() {
							var text = $(this).text();
							completeValues.push(text);
						});
					}
					else if(ct.indexOf('json') > -1) {
						for(var item in response) {
							completeValues.push(item);
						}
					}
					else if(ct.indexOf('csv') > -1) {
						completeValues = response.split(",");
					}
					else {
						console.log("Unknown Content Type: " + ct);
					}
				}
			});
		}
		else if(typeof(autoCompleteSource) === "function") {
			completeValues = autoCompleteSource(tagValue);
		}
		else {
			$(this.selector).next().append("Sorry, failed to fetch tag data.");
			return;
		}
		if(!$(this.selector).next().has(".sectionTitle[data-type='suggestions']").length) {
			$(this.selector).next().append('<div class="sectionTitle" data-type="suggestions">'+tagData["name"]+'</div>');
			$(this.selector).next().children("[data-type='suggestions']").css({
				"background-color": "grey",
				"color": "white",
				"padding": "2px",
				"padding-left": "5px"
			});
		}
		for (var i = completeValues.length - 1; i >= 0; i--) {
			var suggestion = completeValues[i];
			$(this.selector).next().children("[data-type='suggestions']").after(
				$("<div />").append(
					$("<span />").text(tagValue).css({
						"color": "green"
					}),
					$("<span />").text(suggestion.replace(tagValue, "")).css({
						"font-weight": "bold"
					})
				// for replacement method:
				//).data("replaceFind", alreadyWritten).data(
				//	"replaceWith", commandAlias + suggestion + " "
				// for insertion method:
				.data("insertText", (commandAlias + suggestion + " ").replace(alreadyWritten, ""))
				.data("cursor", cursorPos)
				.click(function(ev) {
					// for replacement method:
					//var replaceFind = $(this).data("replaceFind");
					//var replaceWith = $(this).data("replaceWith");
					//var input = $(this).parent().prev();
					//input.val(input.val().replace(replaceFind, replaceWith));
					// for insertion method:
					var newText = $(this).data("insertText");
					var cursor = $(this).data("cursor");
					var input = $(this).parent().parent().prev();
					var string = input.val();
					input.val(string.substr(0,cursorPos) + newText + string.substr(cursorPos));
					input.focus().trigger("click");
				}))
			);
		};
	}
	/**
	 * Hides the search results.
	 * @returns {FancyQuery}
	*/
	this.hideResults = function(noFade) {
		if(noFade) {
			$(this.selector).next().hide();
		}
		else {
			$(this.selector).next().delay(100).fadeOut();
		}
		return this;
	}
	/**
	 * Clears the search results
	 * @returns {FancyQuery}
	*/
	this.clearResults = function() {
		$(this.selector).next().html("");
		return this;
	}
	/**
	 * Shows the search result panel.
	 * @returns {FancyQuery}
	*/
	this.showResults = function(noFade) {
		var inputWidth = $(this.selector).width();
		if(noFade) {
			$(this.selector).next().width(inputWidth).show();
		}
		else {
			$(this.selector).next().width(inputWidth).fadeIn();
		}
		return this;
	}
	this.construct(selector, tags);
}